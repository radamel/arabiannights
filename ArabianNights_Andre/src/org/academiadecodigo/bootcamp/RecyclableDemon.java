package org.academiadecodigo.bootcamp;

public class RecyclableDemon extends Genie {

    private boolean isRecycled;

    public RecyclableDemon(int maxNrWishes) {

        super(maxNrWishes, true);
        this.isRecycled = false;
    }

    /*public boolean isRecycled() {
        return isRecycled;
    }
    public void setRecycled(boolean recycled) {
        isRecycled = recycled;
    }*/

    @Override
    public void grantWishes() {

        if (!isRecycled) {
            super.grantWishes();
        } else {
            System.out.println("No more wishes for you from Recyclable Demon!");
        }
    }
    @Override
    public void recycle() {
        if(!isRecycled) {
            isRecycled = true;
            System.out.println("The demon has benn recycled.");
        } else {
            System.out.println("Already been recycled.");
        }
    }


}

