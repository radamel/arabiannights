package org.academiadecodigo.bootcamp;

public class FriendlyGenie extends Genie {

    public FriendlyGenie(int maxNrWishes) {

        super(maxNrWishes, false);
    }

    @Override
    public void grantWishes(){

        if (grantedWishes() < getMaxNrWishes()){
            super.grantWishes();
        } else {
            System.out.println("No more wishes for you from Friendly Genie!");
        }
    }
    @Override
    public void recycle() {
        System.out.println("Friendly genie cannot be recycled.");
    }
}
