package org.academiadecodigo.bootcamp;

public class MagicLamp {

    private String name;
    private int maxNumberOfGenies;

    private int numberOfRubs;
    private int numberOfDemons;
    private int rechargedCount;


    public MagicLamp(String name, int maxNumberOfGenies) {
        this.name = name;
        this.maxNumberOfGenies = maxNumberOfGenies;
        this.numberOfRubs = 0;
        this.rechargedCount = 0;
    }



    public void recycle(Genie genie){
        genie.recycle();
        if(genie.isRecyclable()){
        rechargeTheLamp();
        }
    }

    public void rechargeTheLamp() {
        System.out.println("The magic lamp is recharged.");
        numberOfRubs = 0;
        rechargedCount++;
    }

    public Genie rub() {

        if (numberOfRubs < maxNumberOfGenies) {
            if(numberOfRubs % 2 == 0) {
                System.out.println("New Grumpy Genie");
                numberOfRubs++;
                Genie grumpy = new GrumpyGenie(2);
                return grumpy;
            } else if (numberOfRubs % 2 != 0) {
                System.out.println("New Friendly Genie");
                numberOfRubs++;
                Genie friendly = new FriendlyGenie(2);
                return friendly;
            }
        }
        numberOfRubs++;
        System.out.println("New Recyclable Demon");
        RecyclableDemon demon = new RecyclableDemon(100);
        return demon;
    }
}
