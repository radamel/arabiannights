package org.academiadecodigo.bootcamp;

public class GrumpyGenie extends Genie {

    public GrumpyGenie(int maxNrWishes) {

        super(maxNrWishes, false);
    }

    @Override
    public void grantWishes() {

        if (grantedWishes() < 1) {
            super.grantWishes();
        } else {
            System.out.println("No more wishes for you from Grumpy Genie!");
        }
    }
    @Override
    public void recycle() {
        System.out.println("Friendly genie cannot be recycled.");
    }
}