package org.academiadecodigo.bootcamp;

public class Genie {

    private int maxNrWishes;
    private int grantedWishes;
    private boolean recyclable;

    public Genie(int maxNrWishes, boolean recyclable){
        this.maxNrWishes = maxNrWishes;
        this.grantedWishes = 0;
        this.recyclable = recyclable;
    }

    public int getMaxNrWishes() {
        return maxNrWishes;
    }

    public int grantedWishes() {
        return grantedWishes;
    }

    public void grantWishes(){
        grantedWishes++;
        System.out.println("You were granted a wish.");
    }
    public void recycle() {
        System.out.println("This genie cannot be recycled.");
    }

    public boolean isRecyclable() {
        return recyclable;
    }
}
