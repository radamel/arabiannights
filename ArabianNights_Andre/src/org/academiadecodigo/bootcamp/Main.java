package org.academiadecodigo.bootcamp;

public class Main {

    public static void main(String[] args) {

        MagicLamp amelia = new MagicLamp("Amelia",5);

        Genie genie1 = amelia.rub();
        Genie genie2 = amelia.rub();
        Genie genie3 = amelia.rub();
        Genie genie4 = amelia.rub();
        Genie genie5 = amelia.rub();
        Genie genie6 = amelia.rub();
        Genie genie7 = amelia.rub();
        genie1.grantWishes();
        genie1.grantWishes();
        genie1.grantWishes();
        amelia.recycle(genie5);
        Genie genie8 = amelia.rub();
        amelia.recycle(genie6);
        Genie genie9 = amelia.rub();

    }
}
