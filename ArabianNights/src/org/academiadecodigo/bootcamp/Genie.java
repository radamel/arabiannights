package org.academiadecodigo.bootcamp;

public class Genie {

    private int maxNrWishes;
    private int grantedWishes;

    private boolean isDemon;

    public Genie(int maxNrWishes){
        this.maxNrWishes = maxNrWishes;
    }

    public int getMaxNrWishes() {
        return maxNrWishes;
    }

    public int setMaxWishes(){
        return maxNrWishes;
    }


    public void setDemon(boolean demon) {
        isDemon = demon;
    }

    public boolean checkIfIsDemon() {
        return isDemon;
    }

    public int grantedWishes() {
        return grantedWishes;
    }

    public void grantWishes(){
        grantedWishes++;
        System.out.println("You were granted a wish.");
    }

}
