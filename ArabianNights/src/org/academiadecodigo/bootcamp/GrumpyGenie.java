package org.academiadecodigo.bootcamp;

public class GrumpyGenie extends Genie {

    public GrumpyGenie(int maxNrWishes) {
        super(maxNrWishes);
    }

    @Override
    public void grantWishes(){

        if (grantedWishes() < 1){
            super.grantWishes();
        }
        System.out.println("No more wishes for you from Grumpy Genie!");
    }
}
