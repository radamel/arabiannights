package org.academiadecodigo.bootcamp;

public class RecyclableDemon extends Genie {

    private boolean isRecycled;

    public RecyclableDemon(int maxNrWishes) {
        super(maxNrWishes);
    }

    public boolean isRecycled() {
        return isRecycled;
    }
    public void setRecycled(boolean recycled) {
        isRecycled = recycled;
    }

    @Override
    public void grantWishes(){

        if (isRecycled){
            System.out.println("No more wishes for you from Recyclable Demon!");
        }
        super.grantWishes();
    }

    }

