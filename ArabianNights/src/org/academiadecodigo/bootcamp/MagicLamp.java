package org.academiadecodigo.bootcamp;

public class MagicLamp {

    private String name;
    private int maxNumberOfGenies;

    public static int numberOfRubs;
    public int numberOfDemons;

    public MagicLamp(String name, int maxNumberOfGenies) {
        this.name = name;
        this.maxNumberOfGenies = maxNumberOfGenies;
    }

    public void rechargeTheLamp(RecyclableDemon demon) {
        if (demon.checkIfIsDemon() && demon.isRecycled()) {
            numberOfRubs = 0;
            System.out.println("The magic lamp is recharged.");
        }
    }

    public Genie rub() {

        if (numberOfRubs < maxNumberOfGenies) {
            if(numberOfRubs % 2 != 0) {
                System.out.println("New Grumpy Genie");
                numberOfRubs++;
                Genie grumpy = new GrumpyGenie(2);
                grumpy.setDemon(false);
                return grumpy;
            }else if(numberOfRubs % 2 == 0) {
                System.out.println("New Friendly Genie");
                numberOfRubs++;
                Genie friendly = new FriendlyGenie(2);
                friendly.setDemon(false);
                return friendly;
            }
        }
        numberOfDemons++;
        System.out.println("New Recyclable Demon");
        RecyclableDemon demon = new RecyclableDemon(100);
        demon.setDemon(true);
        return demon;
    }
}
