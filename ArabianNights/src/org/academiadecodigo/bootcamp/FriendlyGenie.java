package org.academiadecodigo.bootcamp;

public class FriendlyGenie extends Genie {

    public FriendlyGenie(int maxNrWishes) {
        super(maxNrWishes);
    }

    @Override
    public void grantWishes(){

        if (grantedWishes() < getMaxNrWishes()){
            super.grantWishes();
        } else {
            System.out.println("No more wishes for you from Friendly Genie!");
        }
    }



}
